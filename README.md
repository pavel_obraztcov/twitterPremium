# twitterPremium: An R Interface to Twitter Premium API

Twitter Premium API provides access to full-history search on Twitter. See
[product comparison](https://developer.twitter.com/en/docs/tweets/search/overview).
This package provides an R interface to the API.

## Dependencies
To install and use the package, the following R packages have to be installed

* `devtools`
* `roxygen2`
* `knitr`
* `rmarkdown`

as well as the following packages that will be installed automatically
* `magrittr`
* `httr`
* `rjson`
* `sodium`
* `openssl`
* `keyring`

## Installation

### Linux & macOS
```{bash}
Rscript -e "install.packages(c('devtools', 'roxygen2', 'knitr', 'rmarkdown'),
  repos = 'https://cloud.r-project.org')"
git clone https://gitlab.com/pavel_obraztcov/twitterPremium.git
cd twitterPremium
Rscript -e "devtools::install(upgrade = 'never', build_vignettes = TRUE)"
```

### Windows
The package has not been tested on Windows.

### Documentation
Start R and run
```{R}
help(package = "twitterPremium", help_type = "html")
```
A short introduction is available in the package vignette.
