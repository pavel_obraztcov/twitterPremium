all: doc install

doc:
	Rscript -e "devtools::document()"

install:
	Rscript -e "devtools::install(upgrade = 'never', build_vignettes = TRUE)"
